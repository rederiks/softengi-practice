import React from "react";
import './header.css';
import {Link, withRouter} from "react-router-dom";
import logo from '../../img/logo.png';

const Header = (props) => {
    const {location} = props;
    if (location.pathname.match("/login") || location.pathname.match("/signup")) {
        return null;
    }
    return(
        <nav className="header">
            <Link to="/"><img src={logo} alt="" className="logo" /></Link>
        </nav>
    );
}

export default withRouter(Header)