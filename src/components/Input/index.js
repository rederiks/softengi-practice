import React from 'react';
import './input.css';

export const Input = (props) => {
    return (
        <div className="form">
            <div>
                <input type={props.type} name={props.name} required></input>
                <label className="input-lable"  autoComplete="off">
                    <span className="input-content">{props.text}</span>
                </label>
            </div>
        </div>
    )
}