import React, {useContext} from 'react';
import { Redirect } from 'react-router';
import {AuthContext} from '../../context/';
import './login.css';
import {Link} from 'react-router-dom';
import { Button, Input, Form } from '../../components';

export const Login = () => {
    

    const {currentUser, handleLogin} = useContext(AuthContext)

    if (currentUser) {
        return <Redirect to="/main"/>;
    }

    return (
        <Form action={handleLogin} text="Log In">
            <Input type="text" name="email" text="Email" />
            <Input type="password" name="password" text="Password" />
            <Button type="submit" text="Log In" />
            <span className="or-break">or</span>
            <Link to="/signup" className="auth-link">Sign Up</Link>
        </Form>
    )
}