import React, {useContext} from 'react';
import './dogs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Spinner, Alert} from './../../components';
import {DogsContext} from './../../context';

export const DogsTab = () => {

    const {dogs, loading} = useContext(DogsContext);
    return(  
        <div className="table-dog">
            {loading ? 
                <Spinner />
                :( dogs == null ?
                    <Alert />
                    :
                    dogs.map((dog, i) => (
                    <div className="cell" key={i}>
                        <img src={dog} alt=''/>
                        <h4>{
                            dog
                                .slice(30)
                                .charAt(0)
                                .toUpperCase() 
                            + 
                            dog
                                .slice(30)
                                .substring(1, dog.slice(30)
                                .indexOf('/'))
                                .replace('-', ' ')
                        }</h4>
                    </div>
                )
                )
                )}
        </div>
    )
}

// @TODO
// Handle null response